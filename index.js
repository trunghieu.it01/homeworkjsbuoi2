function tinhTienLuong() {
  var dailySalaryValue = document.getElementById("dailySalary").value;
  console.log("dailySalaryValue: ", dailySalaryValue);
  var dayValue = document.getElementById("day").value;
  console.log("dayValue: ", dayValue);
  var result;
  result = dailySalaryValue * dayValue;
  console.log("result: ", result);

  var contentResult = `<p> Tiền lương của bạn là: ${result} </p>`;
  document.getElementById("result").innerHTML = contentResult;
}

function tinhGiaTriTrungBinh() {
  var firstNumberValue = document.getElementById("firstNumber").value;
  var secondNumberValue = document.getElementById("secondNumber").value;
  var thirdNumberValue = document.getElementById("thirdNumber").value;
  var fourthNumberValue = document.getElementById("fourthNumber").value;
  var fifthNumberValue = document.getElementById("fifthNumber").value;
  console.log({
    firstNumberValue,
    secondNumberValue,
    thirdNumberValue,
    fourthNumberValue,
    fifthNumberValue,
  });
  var resultEx2 =
    (fifthNumberValue * 1 +
      secondNumberValue * 1 +
      thirdNumberValue * 1 +
      fourthNumberValue * 1 +
      fifthNumberValue * 1) /
    5;
  console.log("resultEx2: ", resultEx2);
  var contentResultEx2 = `<p> Giá trị trung bình của 5 số trên là: ${resultEx2} </p>`;
  document.getElementById("resultEx2").innerHTML = contentResultEx2;
}

function quyDoiNgoaiTe() {
  var dollarValue = document.getElementById("dollar").value;
  var resultEx3 = dollarValue * 23500;
  console.log("resultEx3: ", resultEx3);
  var contentResultEx3 = `<p> Số tiền của bạn sau khi quy đổi $${dollarValue} sang VND là: ${resultEx3} VND </p>`;
  document.getElementById("resultEx3").innerHTML = contentResultEx3;
}

function tinhChuViDienTich() {
  var lengthValue = document.getElementById("length").value;
  var widthValue = document.getElementById("width").value;
  var resultArea;
  resultArea = lengthValue * widthValue;
  console.log("resultArea: ", resultArea);
  resultPerimeter = (lengthValue * 1 + widthValue * 1) * 2;
  console.log("resultPerimeter: ", resultPerimeter);
  var contentResultEx4 = `<p> Chu vi: ${resultPerimeter} </p>
                          <p> Diện tích: ${resultArea} </p>
                          
  `;
  document.getElementById("resultEx4").innerHTML = contentResultEx4;
}
function tinhTongKiSo() {
  var numberInputValue = document.getElementById("numberInput").value;
  var units;
  units = Math.floor(numberInputValue / 10);
  var dozens;
  dozens = numberInputValue % 10;
  var resultEx5 = units + dozens;
  console.log("resultEx5: ", resultEx5);
  var contentResultEx5 = `<p> Tổng 2 kí số của ${numberInputValue} là: ${resultEx5} </p>`;
  document.getElementById("resultEx5").innerHTML = contentResultEx5;
}
